'''
    Programme de lecture et d'analyse d'un fichier structuré (CSV)
    statistuqes : H/F de la promotion
'''
import argparse
import datetime as dt
from dateutil import relativedelta as rd
# Import des fonctons de l'application
import functions.application_functions as af

# Définition de la version du script
SCRIPT_VERSION = 'Version 1.2'


parser = argparse.ArgumentParser()
parser.add_argument("-f", "--path", type=str)
parser.add_argument("-v","--version", action='version', version=f"%(prog)s {SCRIPT_VERSION}")
parser.add_argument("-l","--list", action='store_true', help="Display the list of students")


args = parser.parse_args()
file = args.path

if file == None :
    file="promotion_B3_B.csv"
today=dt.datetime.now()

students = []
nbMoisTot = 0
# Boucle de lecture ligne à ligne
with open(file, 'r') as file :
    nbrf = nbrh = nbro = 0
    # On saute la premier ligne
    next(file)
    for line in file:
        if line[0] == '#':
            continue
        fields = line.strip().split(";")
        gender = fields[2]
        bdate = fields[4]
        bdate = dt.datetime.strptime(bdate,'%d/%m/%Y')
        # delta = rd.relativedelta(today, bdate )
        ans, mois = af.gdelta_age(today, bdate)
        nbMoisTot+=af.nbMonths(today, bdate)
        students.append((fields[1], fields[0], bdate, gender, ans, int(mois)))  # Convertit mois en entier
        print(f"{fields[1]} - {fields[0]} - {bdate} , âge : {ans} ans et {mois} mois")
        if gender.lower() == 'h':
            nbrh+=1
        elif gender.lower() == 'f':
            nbrf+=1
        else:
            nbro+=1

nbr_tot = nbrf + nbrh + nbro

# Calcul de la somme totale des âges en mois pour tous les étudiants
total_age_months = sum(student[4] * 12 + student[5] for student in students)
# Calcul de l'âge moyen en années et mois à partir de la somme totale des âges en mois
age_mean_years, age_mean_months = divmod(total_age_months // nbr_tot, 12)


sep="="*80
print(sep)
if args.list:
    for student in students:
        print(f"{student[1]} - {student[0]} - {student[2].strftime('%d/%m/%Y')}, âge : {student[4]} ans et {student[5]} mois")
    print ("\n")

print(f"Nombre total d'élèves : {nbr_tot}")
print(f"Nombre total de filles : {nbrf} - {af.cpercent(nbrf, nbr_tot, 2)} %")
print(f"Nombre total de garçons : {nbrh} - {af.cpercent(nbrh, nbr_tot, 2)} %")
print(f"Autres : {nbro} - {af.cpercent(nbro, nbr_tot, 2)} %")

# On calule l'âge moyen de la  promotion à partir de total des mois
nbMois=int(nbMoisTot/nbr_tot)
nbAnM=nbMois//12
nbMoisM=nbMois % 12


print(f"Age moyen de la promotion : {age_mean_years} ans et {age_mean_months} mois")
print(sep)
print("fin du programme...")
    
